import React from 'react'
import {View, Text, SafeAreaView, Image, Alert} from 'react-native'
import { Input, Button } from '../../components'
import { colors } from '../../utils'
import { OjolRegister } from '../../assets'

const Register = () => {
    return (
        <View style={{ backgroundColor:'white',flex:1}}>
                <View style={{alignItems:'center', marginTop:50, justifyContent:'center' }}>
                    <Image source={OjolRegister} style={{width:166, height:175}}/>
                    <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:16,maxWidth:300, textAlign:'center'}}>Mohon mengisi beberapa data untuk proses daftar anda</Text>
                    <View style={styles.space(64)}/>
                    <Input placeholder="Masukkan Nama Lengkap"/>
                    <View style={styles.space(33)}/>
                    <Input placeholder="Masukkan Email"/>
                    <View style={styles.space(33)}/>
                    <Input placeholder="Masukkan Password" />
                    <View style={styles.space(83)}/>
                    <Button title="Daftar Sekarang" onPress={()=> alert('Terimakasih Sudah Mendaftar Silahkan Login')}/>
                </View>
        </View>
    )
}

const styles = {
    space:value=> {
        return{
            height:value
        }
    }
}
export default Register;