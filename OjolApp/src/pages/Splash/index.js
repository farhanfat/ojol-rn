import React, { useEffect } from 'react'
import {View, Text, SafeAreaView} from 'react-native'
import { colors } from '../../utils';


const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(()=> {
            navigation.replace('WelcomeAuth');
        }, 2000);
    });
    
    return (
        // <SafeAreaView>
            <View style={{
                backgroundColor:colors.default, 
                flex:1, 
                alignItems:'center', 
                justifyContent:'center', 
            }}>
                <Text style={{
                    color:'white', 
                    fontSize: 50, 
                    fontStyle:'italic'
                }} >
                    OJOL APPS
                </Text>
                <Text style={{
                    color:'white',fontSize: 18, 
                }}>
                    Made By : Farhan Faturohman
                </Text>
            </View>
        // </SafeAreaView>
            
        
    );
};

export default Splash;