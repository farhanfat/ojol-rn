import React from 'react'
import {View, Text, SafeAreaView, Image} from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import ActionButton from './ActionButton'
import { colors } from '../../utils'
import { welcomeAuth } from '../../assets'

const WelcomeAuth = ({navigation}) => {
    const handleGoTo = (screen) => {
        navigation.navigate(screen);
    };
    return (
        <SafeAreaView style={styles.wrapper.page}>
            <Image source ={welcomeAuth} style={styles.wrapper.illustration}/>
            <Text style={styles.text.welcome}>
                Selamat Datang di OJOL
            </Text>
            <ActionButton 
                desc="Silahkan masuk, jika anda memiliki akun" 
                title="Masuk"
                onPress={()=>handleGoTo('Login')}
            />
            <ActionButton 
                desc="atau Silahkan Daftar, jika anda belum memiliki akun" 
                title="Daftar"
                onPress={()=>handleGoTo('Register')}
            />
        </SafeAreaView>
    )
}

const styles = {
    wrapper:{
        page:{
            alignItems:'center', 
            justifyContent:'center', 
            backgroundColor:'white',
            flex:1
        },
        illustration:{
            width:219, 
            height: 217, 
            marginBottom:10
        }
    },
    text:{
        welcome:{
            fontSize:18,
            fontWeight:'bold',
            color: colors.default, 
            marginBottom:76
        }
    }
}

export default WelcomeAuth;