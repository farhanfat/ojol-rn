import React from 'react'
import {View, Text, SafeAreaView, Image, Alert} from 'react-native'
import { Input, Button } from '../../components'
import { colors } from '../../utils'
import { OjolRegister } from '../../assets'
import { LoginGambar } from '../../assets'

const Login = ({navigation}) => {
    const handleGoTo = (screen) => {
        navigation.navigate(screen);
    };

    loginHandler = (email,password) => {
        console.log(this.state.email, ' ', this.state.password)
        if(password=='12345678'){
          handleGoTo('Home')
        }
        else {
          alert('Password Anda Salah')
        }
      };
    
    return (
        <View style={{ backgroundColor:'white',flex:1}}>
        <View style={{alignItems:'center', marginTop:50, justifyContent:'center' }}>
            <Image source={LoginGambar} style={{width:266, height:275}}/>
            <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:16,maxWidth:300, textAlign:'center'}}>Masukkan Email dan Password yang terdaftar untuk masuk ke dalam sistem</Text>
            <View style={styles.space(64)}/>
            <Input placeholder="Masukkan Email" onChangeText={email => this.setState({ email })}/>
            <View style={styles.space(33)}/>
            <Input placeholder="Masukkan Password" onChangeText={password => this.setState({ password })}/>
            <View style={styles.space(83)}/>
            <Button title="Masuk" onPress={()=>handleGoTo('Home')}/>
        </View>
</View>
    )
}

const styles = {
    space:value=> {
        return{
            height:value
        }
    }
}

export default Login;