import React from 'react'
import {View, Text, SafeAreaView, Image, Alert} from 'react-native'
import { Input, Button } from '../../components'
import { colors } from '../../utils'
import { OjolRegister } from '../../assets'
import { LoginGambar } from '../../assets'
import { data } from './data.json'
import { FlatList } from 'react-native-gesture-handler'
import Farhan from './FarhanPNG3.png'


const Login = ({navigation}) => {
    const handleGoTo = (screen) => {
        navigation.navigate(screen);
    };

    
    return (
        <View style={{ backgroundColor:'white',flex:1}}>
            <View style={{alignItems:'center', marginTop:50, justifyContent:'center' }}>
                <Image source={Farhan}></Image>
            </View>
            <Text style={{color:colors.default, textAlign:'center', fontSize:70}}>Farhan Faturohman</Text>
        </View>
    )
}

const styles = {
    space:value=> {
        return{
            height:value
        }
    }
}

export default Login;