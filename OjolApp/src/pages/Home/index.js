import React from 'react'
import {View, Text, SafeAreaView, Image, Alert} from 'react-native'
import { Input, Button } from '../../components'
import { colors } from '../../utils'
import { OjolRegister } from '../../assets'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'

const Home = ({navigation}) => {
    const handleGoTo = (screen) => {
        navigation.navigate(screen);
    };
    return (
        <View style={{ backgroundColor:'white',flex:1}}>
        <SafeAreaView  style={{alignItems:'center', marginTop:30}}>
        <Text style={{fontSize: 16, fontWeight:'bold', color:colors.default,marginTop:16,marginBottom:10,maxWidth:320, textAlign:'center'}}>Mari Memilih Ojek Anda untuk dipanggil ke posisi Anda</Text>
            <ScrollView style={{height:750}}>
                <View style={{
                    margin:10, 
                    backgroundColor:'white',
                    borderRadius:20, 
                    shadowColor: "#000000",
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    },
                    maxWidth:400,
                    maxHeight:320
                }}>
                    <Image source={{uri:'https://cdn-image.hipwee.com/wp-content/uploads/2018/11/hipwee-ojek-online-tuyul-online-750x422.jpg'}} style={{width:400,height:200,borderTopLeftRadius:20,borderTopRightRadius:20}}/>
                    <View style={{paddingBottom:120}}>
                        <Text style={{fontSize: 16, fontWeight:'bold', color:colors.default,marginTop:10,textAlign:'center'}}>Bambang Sarjono, S.Kom., M.Msi</Text>
                        <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:10, marginBottom:15,textAlign:'center'}}>Naik Motor saya serasa naik mobil cihuy</Text>
                        <Button title="Order Sekarang" onPress={()=>handleGoTo('Detail')}/>
                    </View>
                </View>

                <View style={{
                    margin:10, 
                    backgroundColor:'white',
                    borderRadius:20, 
                    shadowColor: "#000000",
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    },
                    maxWidth:400,
                    maxHeight:320
                }}>
                    <Image source={{uri:'https://cdn1-production-images-kly.akamaized.net/-4DbdimlXWL7q-YWWjeSfwS5lYc=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2985940/original/091581800_1575433832-WhatsApp_Image_2019-12-04_at_11.13.07.jpeg'}} style={{width:400,height:200,borderTopLeftRadius:20,borderTopRightRadius:20}}/>
                    <View style={{paddingBottom:120}}>
                        <Text style={{fontSize: 16, fontWeight:'bold', color:colors.default,marginTop:10,textAlign:'center'}}>Suherandaru Wahdarai</Text>
                        <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:10, marginBottom:15,textAlign:'center'}}>Naik Ojek bersama saya, saya ajak ngebut</Text>
                        <Button title="Order Sekarang" onPress={()=>handleGoTo('Detail')}/>
                    </View>
                </View>
                

                <View style={{
                    margin:10, 
                    backgroundColor:'white',
                    borderRadius:20, 
                    shadowColor: "#000000",
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    },
                    maxWidth:400,
                    maxHeight:320
                }}>
                    <Image source={{uri:'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSJNBs7CIIlRTThx3Jl_xX5v3fptrPWVUWczw&usqp=CAU'}} style={{width:400,height:200,borderTopLeftRadius:20,borderTopRightRadius:20}}/>
                    <View style={{paddingBottom:120}}>
                        <Text style={{fontSize: 16, fontWeight:'bold', color:colors.default,marginTop:10,textAlign:'center'}}>Syahrul Gunawan</Text>
                        <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:10, marginBottom:15,textAlign:'center'}}>Beatku adalah dirimu</Text>
                        <Button title="Order Sekarang" onPress={()=>handleGoTo('Detail')}/>
                    </View>
                </View>

                <View style={{
                    margin:10, 
                    backgroundColor:'white',
                    borderRadius:20, 
                    shadowColor: "#000000",
                    shadowOpacity: 0.8,
                    shadowRadius: 2,
                    shadowOffset: {
                        height: 1,
                        width: 1
                    },
                    maxWidth:400,
                    maxHeight:320
                }}>
                    <Image source={{uri:'https://cdn2.tstatic.net/bangka/foto/bank/images/divani-gojek-cantik-yang23222.jpg'}} style={{width:400,height:200,borderTopLeftRadius:20,borderTopRightRadius:20}}/>
                    <View style={{paddingBottom:120}}>
                        <Text style={{fontSize: 16, fontWeight:'bold', color:colors.default,marginTop:10,textAlign:'center'}}>Restiana</Text>
                        <Text style={{fontSize: 14, fontWeight:'bold', color:colors.default,marginTop:10, marginBottom:15,textAlign:'center'}}>Rider Cantik</Text>
                        <Button title="Order Sekarang" onPress={()=>handleGoTo('Detail')}/>
                    </View>
                </View>
            </ScrollView>
            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem} onPress={()=>handleGoTo('Profil')}>
                    {/* <Icon name ="whatshot" size={25}/> */}
                    <Text style={styles.tabTitle}>Pembuat Aplikasi</Text>
                    
                </TouchableOpacity>
            </View>
        </SafeAreaView>
        
    </View>
    )
}

const styles = {
    wrapper:{
        page:{
            alignItems:'center', 
            justifyContent:'center', 
            backgroundColor:'white',
            flex:1
        }
    },
    navBar : {
            height : 95,
            backgroundColor : 'white',
            shadowColor: "#000",
            shadowOffset: {
            width: 0,
            height: 2,
        }
    },
    tabBar :{
              flexDirection:'row',
              backgroundColor:'white',
              borderTopWidth:0.5,
              borderColor:'#E5E5E5',
              height : 60,
              shadowRadius: 2,
              shadowOpacity: 0.1,
              shadowOffset: {
                width: 0,
                height: -3,
              },
              shadowColor: '#000000',
              elevation: 4,
              justifyContent:'space-around',
              
    },
    tabItem:{
            alignItems:'center',
            justifyContent:'center'
        },
    tabTitle:{
            fontSize:11,
            paddingTop:4,
            color:'#3c3c3c'
        }
        
}


// const styles = StyleSheet.create({
//     container: {
//       flex: 1
//     },
//     navBar : {
//       height : 95,
//       backgroundColor : 'white',
//       shadowColor: "#000",
//       shadowOffset: {
//         width: 0,
//         height: 2,
//       },
//       shadowOpacity: 0.25,
//       shadowRadius: 3.84,
//       elevation: 5,
//       flexDirection : 'row',
//       alignItems :'center',
//       justifyContent :'space-between'
//     },
//     rightNav :{
//       flexDirection:'row'
//     },
//     body :{
//       flex:1
//     },
//     tabBar :{
//       flexDirection:'row',
//       backgroundColor:'white',
//       borderTopWidth:0.5,
//       borderColor:'#E5E5E5',
//       height : 60,
//       shadowRadius: 2,
//       shadowOpacity: 0.1,
//       shadowOffset: {
//         width: 0,
//         height: -3,
//       },
//       shadowColor: '#000000',
//       elevation: 4,
//       justifyContent:'space-around'
//     },
//     tabItem:{
//       alignItems:'center',
//       justifyContent:'center'
//     },
//     tabTitle:{
//       fontSize:11,
//       paddingTop:4,
//       color:'#3c3c3c'
//     }
//   });

export default Home;