import Splash from './Splash'
import Login from './Login'
import Register from './Register'
import WelcomeAuth from './WelcomeAuth'
import Home from './Home'
import Detail from './Detail'
import Profil from './Profil'


export {Splash, Login, Register, WelcomeAuth, Home, Detail,Profil};