export const colors = {
    default:'#00CC00',
    disable:'#A5B1C2',
    dark:'#474747',
    text:{
        default: '#7e7e7e'
    }
};

